//Importing Node modules and initializing Express
const express = require('express'),
    app = express(),
    logger = require('morgan'),
    path = require('path'),
    http = require('http');

var server = http.createServer(app).listen(process.env.PORT || 4730);
console.log('Server is running on port: ' + process.env.PORT || 4730);

// Setting up basic middleware for all Express requests
app.use(logger('dev')); // Log requests to API using morgan

// Enable CORS from client-side
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

//Serving Angular content 
app.use(express.static(path.join(__dirname, '/app')));
app.use('/bower_components', express.static(path.join(__dirname, '/bower_components')));
app.all('/*', function (req, res, next) {
    res.sendFile(path.join(__dirname, '/app/index.html'));
});
