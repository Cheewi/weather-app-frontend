'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('MainCtrl', ['$scope', 'WeatherService', function ($scope, WeatherService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.currentNavItem = 'page1';
    $scope.city = 'irvine';
    $scope.date = new Date();
    $scope.cityError = false;
    
    getWeatherInfo($scope.city);

    //Lookup weather for a city
    $scope.getWeather = function () {
      // $scope.city = $scope.inputCity;
      getWeatherInfo($scope.inputCity);
      $scope.inputCity = null;
    }

    function getWeatherInfo(city) {
      //Get current temperature
      WeatherService.getCurrentWeather(city)
        .then(function (data) {
          if(data.currentWeather && data.currentWeather.cod === '404'){
            $scope.cityError = true;
          }
          else if(data.currentWeather){
            $scope.cityError = false;
            $scope.city = data.currentWeather.name;
            $scope.currentWeather = data.currentWeather;
            console.log($scope.currentWeather);

            //Get forecast after getting the generic data
            WeatherService.getWeatherForecast(city)
              .then(function (data) {
                $scope.weatherForecast = data.weatherForecast;
                console.log($scope.weatherForecast);
              });
          }
        });

    }
  }]);