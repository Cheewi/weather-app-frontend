'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:ForecastCtrl
 * @description
 * # ForecastCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('ForecastCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
