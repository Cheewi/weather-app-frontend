'use strict';

/**
 * @ngdoc service
 * @name publicApp.weather
 * @description
 * # weather
 * Service in the publicApp.
 */
angular.module('publicApp')
  .service('WeatherService', ['$http', 'appConfig', function ($http, appConfig) {
    
    return {
      getCurrentWeather: function (data, success, error) {
        return $http({
          method: 'GET',
          url: appConfig.apiUrl + '/temperature/' + data,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response) {
          return response.data;
        }, function(error) {
          return error;
        });
      },

      getWeatherForecast: function (data, success, error) {
        return $http({
          method: 'GET',
          url: appConfig.apiUrl + '/forecast/' + data,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response) {
          return response.data;
        }, function(error) {
          return error;
        });
      }
    }
  }]);
